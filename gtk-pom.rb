#!/usr/bin/env ruby

# Require gtk, fileutils
require 'gtk3'
require 'fileutils'

app = Gtk::Application.new 'com.jamespdev.gtk-pom', :flags_none

app.signal_connect :activate do |application|
  window = Gtk::ApplicationWindow.new(application)
  window.set_title 'Hello Ruby!'
  window.present
end

puts app.run