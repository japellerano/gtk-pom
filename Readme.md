# Todo and Pomadoro Timer

Working on my ruby and gtk integration skills, building off of a previous tutorial.  

Here is the tutorial: [https://iridakos.com/tutorials/2018/01/25/creating-a-gtk-todo-application-with-ruby](https://iridakos.com/tutorials/2018/01/25/creating-a-gtk-todo-application-with-ruby)  